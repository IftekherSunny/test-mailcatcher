<?php 

$I = new FunctionalTester($scenario);

// arrange
$I->am('a user');
$I->wantTo('login to my sun hospital suite account');
$I->haveAnAccount();

// act
$I->amOnPage('/auth/login');
$I->fillField('email', 'sunny@gmail.com');
$I->fillField('password', 'sunny');
$I->click('Login');

// assert
$I->seeCurrentUrlEquals('/');
$I->assertTrue(Auth::check());