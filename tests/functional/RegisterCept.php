<?php 
$I = new FunctionalTester($scenario);
$I->wantTo('create an account in the system');
//$I->beginTransaction();


$I->amOnPage('/auth/register');

$form = [
     'name' => 'sunny',
     'email' => 'sunny@gmail.com',
     'password' => 'sunnys',
     'password_confirmation' => 'sunnys'
];
$I->submitForm('form', $form, 'Register');
$I->seeCurrentUrlEquals('/');
$I->seeRecord('users', array('name' => 'sunny'));


$I->seeInLastEmail("Please click this link to reset your password");