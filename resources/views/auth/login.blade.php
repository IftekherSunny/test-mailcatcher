@if($errors)
    @foreach($errors->all() as $error)
        <li>{{ $error }}</li>
    @endforeach
@endif


<form method="POST" action="/auth/login">
    {!! csrf_field() !!}

    <div>
        Email
        <input type="email" name="email" value="{{ old('email') }}">
    </div>

    <div>
        Password
        <input type="password" name="password" id="password">
    </div>

    <div>
        <input type="checkbox" name="remember"> Remember Me
    </div>

    <div>
        <input type="submit" value="Login" />
    </div>
</form>